# Коллекция стикеров для Telegram

Как их делать читайте в [wiki](https://github.com/free-belarus/telegram-stickers/wiki).

- [Лукашенко 3%аебал](https://t.me/addstickers/Lukashenko_Zaebal)
- [История национального флага Беларуси](https://t.me/addstickers/A_brief_history_of_flag_of_Belarus)
- [Плакаты с митингов](https://telegram.me/addstickers/Posters_of_Free_Belarus)
